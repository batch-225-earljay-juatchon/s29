db.users.insertMany([
			{
				firstName: "Jane",
				lastName: "Doe",
				age: 21,
				contact: {
				    phone: "87654321",
				    email: "janedoe@gmail.com"
				},
				courses: [ "CSS", "Javascript", "Python" ],
				department: "HR"
			},
		    {
		        firstName: "Stephen",
		        lastName: "Hawking",
		        age: 50,
		        contact: {
		            phone: "87654321",
		            email: "stephenhawking@gmail.com"
		        },
		        courses: [ "Python", "React", "PHP" ],
		        department: "HR"
		    },
		    {
		        firstName: "Neil",
		        lastName: "Armstrong",
		        age: 82,
		        contact: {
		            phone: "87654321",
		            email: "neilarmstrong@gmail.com"
		        },
		        courses: [ "React", "Laravel", "Sass" ],
		        department: "HR"
		    }
		]);



db.users.find({$or: 
    
    [   { firstName: {$regex: 's', $options: '$i' } }, 
    
        { lastName: {$regex:'d', $options: '$i'} }
    ] 
            },

    {   
         firstName: 1,
         lastName: 1,
         _id: 0
    });





db.users.find({ age : { $gt : 70} });
db.users.find({ age : { $gte: 70} });





db.users.find( {$and: [ { firstName: {$regex: 'e', $options: '$i' } }, {age:{ $lte:30 }} ] } );