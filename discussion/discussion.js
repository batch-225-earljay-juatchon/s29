//  MongoDB - Query Operators and Field Projection


db.users.insertMany([
			{
				firstName: "Jane",
				lastName: "Doe",
				age: 21,
				contact: {
				    phone: "87654321",
				    email: "janedoe@gmail.com"
				},
				courses: [ "CSS", "Javascript", "Python" ],
				department: "none"
			},
		    {
		        firstName: "Stephen",
		        lastName: "Hawking",
		        age: 50,
		        contact: {
		            phone: "87654321",
		            email: "stephenhawking@gmail.com"
		        },
		        courses: [ "Python", "React", "PHP" ],
		        department: "none"
		    },
		    {
		        firstName: "Neil",
		        lastName: "Armstrong",
		        age: 82,
		        contact: {
		            phone: "87654321",
		            email: "neilarmstrong@gmail.com"
		        },
		        courses: [ "React", "Laravel", "Sass" ],
		        department: "none"
		    }
		]);

//  [SECTION] Comparison Query Operators

//  $gt/$gte operator
// - Allows us find documents that have field number value greater than or equal to a specified value.

db.users.find({ age : { $gt : 50} });
db.users.find({ age : { $gte : 50} });

// $lt/lte operator
// - Allows us find documents that have field number value less than or equal to a specified value.

db.users.find({ age : { $lt : 50} });
db.users.find({ age : { $lte : 50} });

// $ne operator - Allows us find documents that have field number value not equal to a specified value.

db.users.find({ age : { $ne : 82} });

// $in operator
/*
	-Allows us to find documents with a specific match criteria one field using differend values.

	-Syntax
		db.collectionName.find({field : { $in : value }});
*/

db.users.find( { lastname: { $in: ["Hawking", "Doe"] }});
db.users.find( { courses: { $in: ["HTML", "React"] }});

// [SECTION] Logical Query Operators
// $or operator
/*
	- Allows us to find documents that match a single criteria from multiple provided search criteria.
	- Syntax
		db.collectionName.find({ $or: [ { fieldA: valueB }, { fieldB: valueB } ] });
*/
db.users.find( {$or: [ { firstName: "Neil" }, { age : 50 } ] });
db.users.find( {$or: [ { firstName: "Neil" }, { age : { $gt: 30} }] });

// $and operator
/*
	-Allows us to find documents matching multiple criteria in a single field.
*/

db.users.find({ $and: [{ age : { $ne: 82}, { age : { $ne: 50} } }] });

// [SECTION] Field Projection
/*
	- Retrieving documents are common operators that we do and by default MongoDB queries return the whole document as a response.
	- When dealing with complex data structures, there might be instances when fields not useful for the query that we are tryinh to accomplish.
	- To help with readability of the values returned, we can include/exclude fields from the response.
*/

//  Inclusion
/*
	- Allows us to include/add specific fields only when retrieving documents.
	- The value provided is 1 to True denote that field is being included

		Syntax:
			db.users.find( {criteria}, {field: 1})
*/

db.users.find(
		{
			firstName: "Jane"
		},
		{
			firstName: 1,
			lastName: 1,
			contact: 1
		}
	);

// Exclusion
/*
	- Allows us to exclude/remove specific fields only when retrieving documents.
	- The value provided is 0 to false denote that field is being included

		Syntax:
			db.users.find( {criteria}, {field: 1})
*/

db.users.find(
{
	firstName: "Stephen"
},
{
	contact: 0,
	department: 0
}

	);

// [SECTION] Evaluation Query Operators 

 // $regex operator
 /*
	- Allows to find documents that match a specific string pattern using regular expressions.
	- $options: '$i' - is for Case insensitivity to much upper and lower cases.

	- $options: "Extended" capability to ignore all white space characters in the $regex pattern unless escaped or include in a character class

	-$options: '$x' - "Extended" capability to ignore all white space characters in the $regex pattern unless escaped or included in a character class
 */ 

//  Case sensitive query
db.users.find({ firstName: { $regex: 'N'}} );

// Case insensitive query
db.users.find({firstName: { $regex: 'j', $options: '$i'}});
